package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

public class WeatherResponse {

    public final Main main = null;
    public final Condition condition= null;
    public final Wind wind = null;
    public final Clouds clouds = null;

    public static class Main{
        public final Float temp = 0.0f;
        public final Integer humidity = null;

    }

    public static class Condition{
        public final String description = null;
        public final String icon = null;

    }

    public static class Clouds{
        public final Integer all = null;
    }

    public final Integer dt = null;


    public final List<Condition> weather = null;

    public static class Wind{
        public final Float speed = 0.0f;
    }

}
