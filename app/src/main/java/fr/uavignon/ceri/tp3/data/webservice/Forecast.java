package fr.uavignon.ceri.tp3.data.webservice;

public class Forecast {
    public final String name;
    public final float temperature;
    public final float humidite;
    public final float vent;
    public final String nubelosite;

    public Forecast(String name, float temperature, float humidite, float vent, String nubelosite) {
        this.name = name;
        this.temperature = temperature;
        this.humidite = humidite;
        this.vent = vent;
        this.nubelosite = nubelosite;
    }
}
